package com.cubisoft.nfcwriter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    StringBuilder rawPayloadBuilder;
    int requestMode = -1;
    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    String callsign;
    List<String> acciones;
    App app;
    private static final int PERMISSION_STORAGE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        app = (App) getApplicationContext();
        checkPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,0);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        setTitle(BuildConfig.VERSION_NAME);
        callsign = app.getPrefs().getString("callsign","BT00000000");
        if(nfcAdapter == null){
            Toast.makeText(this,"No hay NFC",Toast.LENGTH_SHORT).show();
            return;
        }
        pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, this.getClass())
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);

        ListView listView = findViewById(R.id.lista);
        acciones = new ArrayList<>();
//        acciones.add("Petición estado bateria");
        acciones.add("Confirmación estado batería");
        acciones.add("Codigo de envio");
        acciones.add("Trama 1");
        acciones.add("Trama 2");
        acciones.add("Trama 3");
        acciones.add("Trama 4");
        acciones.add("Trama 5");
        acciones.add("Trama corta ejemplo");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,acciones);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                rawPayloadBuilder = new StringBuilder();
                requestMode = i+1;
                if(i==1){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    LayoutInflater layoutInflater = getLayoutInflater();
                    @SuppressLint("InflateParams")
                    View dialogView = layoutInflater.inflate(R.layout.dialog, null);
                    final EditText dStart = dialogView.findViewById(R.id.et_mostrar_fecha_picker);
                    dStart.setText(callsign);
                    builder.setView(dialogView)
                            .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int i) {
                                    if((!dStart.getText().toString().equals(""))) {
                                        try {
                                            callsign = dStart.getText().toString();
                                            app.editSharedPreference("callsign", callsign);
                                            dialog.dismiss();
                                        } catch (Exception e) {
                                            App.getContext().appendException(e);
                                            dialog.dismiss();
                                        }
                                    }
                                }
                    });
                    builder.create().show();
                }
//                switch (i){
//                    case 0:
//                        String tagId = new String(myTag.getId(),"ISO-8859-1");
//                        currentCallSign = Converter.asciiToHex(tagId);
//                        rawPayloadBuilder.append(Constants.getBatteryConfirm("FFFF"));
//                        break;
//                    default:
//                        try {
//                            rawPayloadBuilder.append(Converter.fakeRecords(i - 1));
//                        } catch (Exception e){
//                            Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
//                        }
//                        break;
//                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(nfcAdapter != null) {
            if (!nfcAdapter.isEnabled()) {
                displayWirelessSettings();
            }
            nfcAdapter.enableForegroundDispatch(this, pendingIntent,null, null);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(nfcAdapter != null) {
            if (nfcAdapter.isEnabled()) {
                nfcAdapter.disableForegroundDispatch(this);
            }
        }
    }

    private void displayWirelessSettings(){
        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        resolveIntent(intent);
    }

    void resolveIntent(Intent intent){
        String action = intent.getAction();
        rawPayloadBuilder = new StringBuilder();
        if (action != null) {
            switch (action) {
                case NfcAdapter.ACTION_NDEF_DISCOVERED:
                case NfcAdapter.ACTION_TAG_DISCOVERED:
                case NfcAdapter.ACTION_TECH_DISCOVERED:
                    try {
                        if(requestMode == 1){
                            rawPayloadBuilder.append(Constants.getBatteryConfirm(Converter.asciiToHex(callsign)));
//                        } else if (requestMode == 8){
//                            rawPayloadBuilder.append(Converter.shortRecords(requestMode-3));
                        }
                        else
                            rawPayloadBuilder.append(Converter.fakeRecords(requestMode-3));
                        writeTag(rawPayloadBuilder.toString());
                        Toast.makeText(this, "Exito opcion "+acciones.get(requestMode-1), Toast.LENGTH_SHORT).show();
                    } catch (Exception e){
                        app.appendException(e);
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
            }
        }
    }

    void writeTag(String message) throws IOException, FormatException {
        byte[] empty = new byte[0];
        Tag tag = getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
        byte[] payload = Converter.toAsciiBytes(message);
        NdefRecord ndefRecord = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, payload);
        NdefMessage msg = new NdefMessage(new NdefRecord[]{ndefRecord});
        Ndef ndefTag = Ndef.get(tag);
        if (ndefTag == null) {
            // Let's try to format the Tag in NDEF
            NdefFormatable nForm = NdefFormatable.get(tag);
            if (nForm != null) {
                nForm.connect();
                nForm.format(msg);
                nForm.close();
            }
        } else {
            ndefTag.connect();
            ndefTag.writeNdefMessage(msg);
            ndefTag.close();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Toast.makeText(getApplicationContext(), "Saving FAILED!", Toast.LENGTH_SHORT)
                        .show();
                finish();
            }
        }
    }

    protected boolean checkPermissions(String permit, int requestCode){
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                permit);
        if(permissionCheck != PackageManager.PERMISSION_GRANTED){
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    permit)) {

            // Show an expanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

//            } else {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this,
                    new String[]{permit},
                    requestCode);

            // MY_PERMISSIONS_REQUEST_INTERNET is an
            // app-defined int constant. The callback method gets the
            // result of the request.
            return false;
//            }

        } else return true;
    }
}
