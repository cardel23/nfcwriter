package com.cubisoft.nfcwriter;

import android.annotation.SuppressLint;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by pwk04 on 06-07-19.
 */

public class Converter {

    public static HashMap<Float, String> map;
    public static final int LAT = 0, LON = 1, CURRENT = 2, PAGES = 4;

//    public static String convertMessage(String inputString) throws Exception {
//        StringBuilder sb = new StringBuilder();
//        String sub = null;
////		sub = inputString.substring(0, 8);
//        int startPosition = 50; //Desde donde empiezan las tramas segun documentacion
//        boolean isEOF = false;
//        while(!isEOF) {
//            sb.append(getConvertedDate(inputString.substring(startPosition+0, startPosition+8))+"\n");
//            sb.append(getConvertedTemp(inputString.substring(startPosition+8,startPosition+12))+"\n");
//            sb.append(dms2dec(inputString.substring(startPosition+12, startPosition+14), inputString.substring(startPosition+14, startPosition+18), Converter.LAT)+"\n");
//            sb.append(dms2dec(inputString.substring(startPosition+18, startPosition+20), inputString.substring(startPosition+20, startPosition+24), Converter.LON)+"\n");
//            if(inputString.substring(startPosition+24,startPosition+26).equals("FE"))
//                isEOF=true;
//            else
//                startPosition += 38;
////			System.out.println(sb.toString());
//        }
//        return sb.toString();
//    }

    public static int getRecordMax(String inputString, int index){
        String str = inputString.substring(index-2, index);
        return Integer.parseInt(str,16);
    }

    public static String getBatteryStatus(String inputString, String callsign){
        StringBuilder sb = new StringBuilder();
        sb.append("Batería del dispositivo al ");
        if(inputString.length() == 12){
            if(inputString.substring(0, 10).equals(Constants.getBatteryConfirm(callsign)))
                sb.append(Integer.parseInt(inputString.substring(10, 12),16));
            else return Constants.BATT_STAT_ERR;
        } else
            return Constants.SIOBEX;
        sb.append(" %");
        return sb.toString();
    }

    public String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
            if (i > 0) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }


    private static String convertDate(long timestamp) {
        return Long.toHexString(timestamp);
    }

    private static String convertTemp(double temp) {
        temp += 273.15;
        int i = Integer.parseInt(
                binTempDecimals(temp), 2);
        return Integer.toString(i, 16);
    }

    private static String convertGPS(double lat, double lon) throws Exception {
        String hexStr = dec2dms(lat, LAT).concat(dec2dms(lon, LON));
        return hexStr;
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

    public static byte[] longToHex(final long l) {
        long v = l & 0xFFFFFFFFFFFFFFFFL;
        byte[] result = new byte[16];
        Arrays.fill(result, 0, result.length, (byte) 0);
        for (int i = 0; i < result.length; i += 2) {
            byte b = (byte) ((v & 0xFF00000000000000L) >> 56);
            byte b2 = (byte) (b & 0x0F);
            byte b1 = (byte) ((b >> 4) & 0x0F);
            if (b1 > 9)
                b1 += 39;
            b1 += 48;
            if (b2 > 9)
                b2 += 39;
            b2 += 48;
            result[i] = (byte) (b1 & 0xFF);
            result[i + 1] = (byte) (b2 & 0xFF);
            v <<= 8;
        }
        return result;
    }

    public static byte[] toAsciiBytes(String hex) throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < hex.length(); i = i + 2) {
            String s = hex.substring(i, i + 2);
            int n = Integer.valueOf(s, 16);
            builder.append((char) n);
        }
        return builder.toString().getBytes("ISO-8859-1");
        // return builder.toString().getBytes();
    }

    public static String asciiToHex(String asciiStr) {
        char[] chars = asciiStr.toCharArray();
        StringBuilder hex = new StringBuilder();
        for (char ch : chars) {
            int i = (int) ch;
            if (i < 16)
                hex.append("0");
            hex.append(Integer.toHexString(i).toUpperCase());
        }
        return hex.toString();
    }

    public static String getEmptyMessage(int maxSize) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        try {
            do{
                sb.append("00");
            } while (toAsciiBytes(sb.toString()).length < maxSize-8);
        } finally {}
        return sb.toString();
    }

    public static String binTempDecimals(double temp) {
        int units = (int) temp;
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        decimalFormat.applyPattern("0.00");
        float decimals = Float.valueOf(decimalFormat.format(temp - units));
//        float decimals = Math.round(temp-units);
//        float decimals = (float) (temp - units);
        if(decimals<0.1){
            decimals += 0.1;
        }
        String binUnits = Integer.toBinaryString(units);
        String binDec = "";
        while (binDec == "") {
            if (decMap().containsKey(decimals)) {
                binDec = decMap().get(decimals);
            } else {
                if (decimals < 0.02f)
                    binDec = "000000";
                else
                    decimals = (float) (decimals - 0.01f);
            }
        }
        return binUnits.concat(binDec);
    }

    private static String dec2dms(double val, int type) throws Exception {
        StringBuilder gpsStr = new StringBuilder();
        int deg = (int) val;
        double min = (Math.abs(val) - Math.abs(deg)) * 60;
        int res = (int) min;
        double sec = (min - res) * 60;
        String minSec = String.valueOf(res).concat(String.valueOf((int)sec));
        int ms = Integer.parseInt(minSec);
        switch (type) {
            case LAT:
                if (deg < 0) {
                    deg = 128 + (deg * -1);
                    if (deg < 128 | deg > 218)
                        throw new Exception("Valor fuera de rango");
                }
                String check = Integer.toHexString(deg);
                while (check.length() < 2)
                    check = "0".concat(check);
                gpsStr.append(check);
                if (ms > 5999) {
                    throw new Exception("Valor fuera de rango");
                } else {
                    check = Integer.toHexString(ms);
                    while (check.length() < 4)
                        check = "0".concat(check);
                    gpsStr.append(check);
                }
                break;
            case LON:
                if (ms > 5999) {
                    throw new Exception("Valor fuera de rango");
                }
                if (deg > 180) {
                    throw new Exception("Valor hexadecimal de grados " + deg + " superior al máximo 180");
                }
                if (deg < 0 & deg >= -180) {
                    deg *= -1;
                    ms += 32768;
                }
                String str = Integer.toHexString(deg);
                while (str.length() < 2)
                    str = "0".concat(str);
                gpsStr.append(str);
                str = Integer.toHexString(ms);
                while (str.length() < 4)
                    str = "0".concat(str);
                gpsStr.append(str);
                break;

        }
        return gpsStr.toString();
    }

    private static HashMap<Float, String> decMap() {
        if (map == null) {
            map = new HashMap<>();
            map.put(0.02f, "000001");
            map.put(0.03f, "000010");
            map.put(0.05f, "000011");
            map.put(0.06f, "000100");
            map.put(0.08f, "000101");
            map.put(0.09f, "000110");
            map.put(0.11f, "000111");
            map.put(0.13f, "001000");
            map.put(0.15f, "001001");
            map.put(0.16f, "001010");
            map.put(0.19f, "001100");
            map.put(0.21f, "001101");
            map.put(0.22f, "001110");
            map.put(0.24f, "001111");
            map.put(0.25f, "010000");
            map.put(0.27f, "010001");
            map.put(0.28f, "010010");
            map.put(0.30f, "010011");
            map.put(0.31f, "010100");
            map.put(0.33f, "010101");
            map.put(0.34f, "010110");
            map.put(0.36f, "010111");
            map.put(0.38f, "011000");
            map.put(0.40f, "011001");
            map.put(0.41f, "011010");
            map.put(0.43f, "011011");
            map.put(0.44f, "011100");
            map.put(0.46f, "011101");
            map.put(0.47f, "011110");
            map.put(0.49f, "011111");
            map.put(0.50f, "100000");
            map.put(0.52f, "100001");
            map.put(0.53f, "100010");
            map.put(0.55f, "100011");
            map.put(0.56f, "100100");
            map.put(0.58f, "100101");
            map.put(0.61f, "100111");
            map.put(0.63f, "101000");
            map.put(0.65f, "101001");
            map.put(0.66f, "101010");
            map.put(0.68f, "101011");
            map.put(0.69f, "101100");
            map.put(0.71f, "101101");
            map.put(0.72f, "101110");
            map.put(0.74f, "101111");
            map.put(0.75f, "110000");
            map.put(0.77f, "110001");
            map.put(0.78f, "110010");
            map.put(0.80f, "110011");
            map.put(0.81f, "110100");
            map.put(0.83f, "110101");
            map.put(0.84f, "110110");
            map.put(0.86f, "110111");
            map.put(0.88f, "111000");
            map.put(0.90f, "111001");
            map.put(0.91f, "111010");
            map.put(0.93f, "111011");
            map.put(0.94f, "111100");
            map.put(0.96f, "111101");
            map.put(0.97f, "111110");
            map.put(0.99f, "111111");
        }
        return map;
    }

    public static String fakeRecords(int selected) throws Exception {
        ArrayList<FakeRecord> a = new ArrayList<>();
        long time = System.currentTimeMillis()/1000;
//        int random = App.getCounter();
        StringBuilder sb = new StringBuilder("");
        int bat = getRandom(1,100);
//        String s = (bat<16)?"0"+Integer.toHexString(bat):Integer.toHexString(bat);
//        sb.append(s).append("FE");
        switch (selected){
            case 0:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.269327, 2.167655));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.236761, 2.274573));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.172485, 2.483010));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.110168, 2.682136));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.048580, 2.883662));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.957122, 3.158998));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.847344, 3.472446));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 39.971483, 5.600456));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 38.927851, 7.495909));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 37.841284, 9.052136));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 36.854858, 10.219602));
                break;
            case 1:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.355371, 0.465546));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.099955, -2.775448));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.451341, -5.938392));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.532177, -13.818908));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.290008, -22.669645));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.306995, -26.110016));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.480390, -35.863314));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.040246, -46.807794));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.589943, -55.339317));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.428715, -70.706642));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.691619, -74.168999));
                break;
            case 2:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.247358, 2.240382));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.213562, 2.349801));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.135516, 2.604889));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.084553, 2.769665));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.025786, 2.959541));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.888103, 3.356054));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.414232, 4.572661));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 39.534600, 6.470656));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 38.406437, 8.276445));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 37.308685, 9.706187));
                break;
            case 3:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.435367, 0.489998));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.252551, -3.778889));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.328957, -9.104096));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.770212, -17.361312));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.257530, -23.001731));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.441518, -31.606272));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.441451, -40.391805));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.700477, -52.978817));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.982124, -60.539820));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.754080, -73.901505));
                break;
            case 4:
//                return "5402656E5CFFA90B4A8E2908F102032051010F5402656E5CFFA90D4A9C2908F903011F51010F5402656E5CFFA90F4A932908FD02042851010F5402656E5CFFA9114A9F2909080105ED51010F5402656E5CFFA9134A972909150106D451010F5402656E5CFFA9154AA129091D0107CF51010F5402656E5CFFA9174A982909250108B951010F5402656E5CFFA9194AA329092C0109A851010F5402656E5CFFA91B4A98290930010A9751010F5402656E5CFFA91D4AA4290939019B86";
                return "05050A515CFFA90B4A8E515CFFA90D4A9C515CFFA90F4A93515CFFA9114A9F515CFFA9134A97515CFFA9154AA1515CFFA9174A98515CFFA9194AA3515CFFA91B4A98515CFFA91D4AA4";
//                return sb.append("0A515CFFA90B4A8E515CFFA90D4A9C515CFFA90F4A93515CFFA9114A9F515CFFA9134A97515CFFA9154AA1515CFFA9174A98515CFFA9194AA3515CFFA91B4A98515CFFA91D4AA4").toString();
            case 5:
                String s = Converter.convertDate(time);
                sb.append("0FFE"+s.toUpperCase()+"FE");
                for(int i = 1;i<=159;i++){
                    if(i<16)
                        sb.append("0");
                    sb.append(Integer.toHexString(i));
                    sb.append(Converter.convertTemp(getRandom(-25.0,-15.0)));
                }
                return sb.toString().toUpperCase();
        }
//        StringBuilder sb = new StringBuilder("");
        int size = a.size();
        int c = 0;
        Iterator<FakeRecord> recordIterator = a.iterator();
//        sb.append("0101");
        sb.append("0").append(selected+1).append("05");
        if(a.size()<16)
            sb.append("0");
        sb.append(Integer.toHexString(a.size()));
        sb.append("51");
        while (recordIterator.hasNext()){
//            sb.append("5402656E");
            FakeRecord r = recordIterator.next();
            sb.append(convertDate(r.time));
            sb.append(convertTemp(r.temp));
//            sb.append(convertGPS(r.lat, r.lon));
            if(c<=size-2){
                sb.append("51");
            }
            c++;
        }

        return sb.toString().toUpperCase();
    }

    public static String shortRecords(int selected) throws Exception {
        long time = System.currentTimeMillis()/1000;
        String s = Converter.convertDate(time);
        return "01FE"+s.toUpperCase()+"FE014A03024A03034A03044A03054A03064A03074A03084A03094A030A4A030B4A030C4A030D4A030E4A030F4A03104A03114A03124A03134A03144A03154A03164A03174A03184A03194A031A4A031B4A031C4A031D4A031E4A031F4A03204A03214A03224A03234A03244A03254A03264A03274A03284A03294A032A4A032B4A032C4A032D4A032E4A032F4A03304A03314A03324A03334A03344A03354A03364A03374A03384A03394A033A4A033B4A033C4A033D4A033E4A033F4A03404A03414A03424A03434A03444A03454A03464A03474A03484A03494A034A4A034B4A034C4A034D4A034E4A034F4A03504A03514A03524A03534A03544A03554A03564A03574A03584A03594A035A4A035B4A035C4A035D4A035E4A035F4A03604A03614A03624A03634A03644A03654A03664A03674A03684A03694A036A4A036B4A036C4A036D4A036E4A036F4A03704A03714A03724A03734A03744A03754A03764A03774A03784A03794A037A4A037B4A037C4A037D4A037E4A037F4A03804A03814A03824A03834A03844A03854A03864A03874A03884A03894A038A4A038B4A038C4A038D4A038E4A038F4A03904A03914A03924A03934A03944A03954A03964A03974A03984A03994A039A4A039B4A039C4A039D4A039E4A039F4A03A04A03A14A03A24A03A34A03A44A03A54A03A64A03A74A03";
    }

    public static int getRandom(int min, int max) {
        int i = new Random().nextInt((max - min) + 1) + min;
        return i;
    }

    @SuppressLint("NewApi")
    public static double getRandom(double min, double max) {
//        DecimalFormat format = new DecimalFormat("#.##");
//        return Double.valueOf(format.format(ThreadLocalRandom.current().nextDouble(min, max)));
        double i = ThreadLocalRandom.current().nextDouble(min, max);
        return i;
    }

}

class FakeRecord{
    public long time;
    public double temp, lat, lon;
    FakeRecord(long time, double temp, double lat, double lon){
        this.time = time;
        this.temp = temp;
        this.lon = lon;
        this.lat = lat;
    }
}
