package com.cubisoft.nfcwriter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    StringBuilder rawPayloadBuilder;
    int requestMode = -1;
    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    String callsign;
    List<String> acciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final App app = (App) getApplicationContext();
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        setTitle(BuildConfig.VERSION_NAME);
        callsign = app.getPrefs().getString("callsign","BT00000000");
        if(nfcAdapter == null){
            Toast.makeText(this,"No hay NFC",Toast.LENGTH_SHORT).show();
            return;
        }
        pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, this.getClass())
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);

        ListView listView = findViewById(R.id.lista);
        acciones = new ArrayList<>();
//        acciones.add("Petición estado bateria");
        acciones.add("Confirmación estado batería");
        acciones.add("Codigo de envio");
        acciones.add("Trama 1");
        acciones.add("Trama 2");
        acciones.add("Trama 3");
        acciones.add("Trama 4");
        acciones.add("Trama 5");
        acciones.add("Trama 6 (1 registro)");
        acciones.add("Trama 7 (2 registros)");
        acciones.add("Trama 8 (89 registros)");
        acciones.add("Trama 9 (58 registros)");
        acciones.add("Trama 10 (120 registros)");
        acciones.add("Trama 11 (120 reg / 75 min)");
        acciones.add("Trama 12 (ejemplo error Jorge");
        acciones.add("Trama 13 (ejemplo correcto Jorge");
        acciones.add("Trama 14 (ejemplo correcto Jorge2");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,acciones);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                rawPayloadBuilder = new StringBuilder();
                requestMode = i+1;
                if(i==1){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    LayoutInflater layoutInflater = getLayoutInflater();
                    @SuppressLint("InflateParams")
                    View dialogView = layoutInflater.inflate(R.layout.dialog, null);
                    final EditText dStart = dialogView.findViewById(R.id.et_mostrar_fecha_picker);
                    dStart.setText(callsign);
                    builder.setView(dialogView)
                            .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int i) {
                                    if((!dStart.getText().toString().equals(""))) {
                                        try {
                                            callsign = dStart.getText().toString();
                                            app.editSharedPreference("callsign", callsign);
                                            dialog.dismiss();
                                        } catch (Exception e) {
                                            dialog.dismiss();
                                        }
                                    }
                                }
                    });
                    builder.create().show();
                }
//                switch (i){
//                    case 0:
//                        String tagId = new String(myTag.getId(),"ISO-8859-1");
//                        currentCallSign = Converter.asciiToHex(tagId);
//                        rawPayloadBuilder.append(Constants.getBatteryConfirm("FFFF"));
//                        break;
//                    default:
//                        try {
//                            rawPayloadBuilder.append(Converter.fakeRecords(i - 1));
//                        } catch (Exception e){
//                            Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
//                        }
//                        break;
//                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(nfcAdapter != null) {
            if (!nfcAdapter.isEnabled()) {
                displayWirelessSettings();
            }
            nfcAdapter.enableForegroundDispatch(this, pendingIntent,null, null);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(nfcAdapter != null) {
            if (nfcAdapter.isEnabled()) {
                nfcAdapter.disableForegroundDispatch(this);
            }
        }
    }

    private void displayWirelessSettings(){
        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        resolveIntent(intent);
    }

    void resolveIntent(Intent intent){
        String action = intent.getAction();
        rawPayloadBuilder = new StringBuilder();
        if (action != null) {
            switch (action) {
                case NfcAdapter.ACTION_NDEF_DISCOVERED:
                case NfcAdapter.ACTION_TAG_DISCOVERED:
                case NfcAdapter.ACTION_TECH_DISCOVERED:
                    try {
                        if(requestMode == 1){
                            rawPayloadBuilder.append(Constants.getBatteryConfirm(Converter.asciiToHex(callsign)));
                        } else
                            rawPayloadBuilder.append(Converter.fakeRecords(requestMode-3));
                        writeTag(rawPayloadBuilder.toString());
                        Toast.makeText(this, "Exito opcion "+acciones.get(requestMode-1), Toast.LENGTH_SHORT).show();
                    } catch (Exception e){
                        App.getContext().appendException(e);
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
            }
        }
    }

    void writeTag(String message) throws IOException, FormatException {
        byte[] empty = new byte[0];
        Tag tag = getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
        byte[] payload = Converter.toAsciiBytes(message);
        NdefRecord ndefRecord = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, payload);
        NdefMessage msg = new NdefMessage(new NdefRecord[]{ndefRecord});
        Ndef ndefTag = Ndef.get(tag);
        if (ndefTag == null) {
            // Let's try to format the Tag in NDEF
            NdefFormatable nForm = NdefFormatable.get(tag);
            if (nForm != null) {
                nForm.connect();
                nForm.format(msg);
                nForm.close();
            }
        } else {
            ndefTag.connect();
            ndefTag.writeNdefMessage(msg);
            ndefTag.close();
        }
    }
}
