package com.cubisoft.nfcwriter;

import android.annotation.SuppressLint;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by pwk04 on 06-07-19.
 */

public class Converter {

    public static HashMap<Float, String> map;
    public static final int LAT = 0, LON = 1, CURRENT = 2, PAGES = 4;

//    public static String convertMessage(String inputString) throws Exception {
//        StringBuilder sb = new StringBuilder();
//        String sub = null;
////		sub = inputString.substring(0, 8);
//        int startPosition = 50; //Desde donde empiezan las tramas segun documentacion
//        boolean isEOF = false;
//        while(!isEOF) {
//            sb.append(getConvertedDate(inputString.substring(startPosition+0, startPosition+8))+"\n");
//            sb.append(getConvertedTemp(inputString.substring(startPosition+8,startPosition+12))+"\n");
//            sb.append(dms2dec(inputString.substring(startPosition+12, startPosition+14), inputString.substring(startPosition+14, startPosition+18), Converter.LAT)+"\n");
//            sb.append(dms2dec(inputString.substring(startPosition+18, startPosition+20), inputString.substring(startPosition+20, startPosition+24), Converter.LON)+"\n");
//            if(inputString.substring(startPosition+24,startPosition+26).equals("FE"))
//                isEOF=true;
//            else
//                startPosition += 38;
////			System.out.println(sb.toString());
//        }
//        return sb.toString();
//    }

    public static int getRecordMax(String inputString, int index){
        String str = inputString.substring(index-2, index);
        return Integer.parseInt(str,16);
    }

    public static String getBatteryStatus(String inputString, String callsign){
        StringBuilder sb = new StringBuilder();
        sb.append("Batería del dispositivo al ");
        if(inputString.length() == 12){
            if(inputString.substring(0, 10).equals(Constants.getBatteryConfirm(callsign)))
                sb.append(Integer.parseInt(inputString.substring(10, 12),16));
            else return Constants.BATT_STAT_ERR;
        } else
            return Constants.SIOBEX;
        sb.append(" %");
        return sb.toString();
    }

    public String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
            if (i > 0) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }
    

    private static String convertDate(long timestamp) {
        return Long.toHexString(timestamp);
    }

    private static String convertTemp(double temp) {
        temp += 273.15;
        int i = Integer.parseInt(
                binTempDecimals(temp), 2);
        return Integer.toString(i, 16);
    }

    private static String convertGPS(double lat, double lon) throws Exception {
        String hexStr = dec2dms(lat, LAT).concat(dec2dms(lon, LON));
        return hexStr;
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

    public static byte[] longToHex(final long l) {
        long v = l & 0xFFFFFFFFFFFFFFFFL;
        byte[] result = new byte[16];
        Arrays.fill(result, 0, result.length, (byte) 0);
        for (int i = 0; i < result.length; i += 2) {
            byte b = (byte) ((v & 0xFF00000000000000L) >> 56);
            byte b2 = (byte) (b & 0x0F);
            byte b1 = (byte) ((b >> 4) & 0x0F);
            if (b1 > 9)
                b1 += 39;
            b1 += 48;
            if (b2 > 9)
                b2 += 39;
            b2 += 48;
            result[i] = (byte) (b1 & 0xFF);
            result[i + 1] = (byte) (b2 & 0xFF);
            v <<= 8;
        }
        return result;
    }

    public static byte[] toAsciiBytes(String hex) throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < hex.length(); i = i + 2) {
            String s = hex.substring(i, i + 2);
            int n = Integer.valueOf(s, 16);
            builder.append((char) n);
        }
        return builder.toString().getBytes("ISO-8859-1");
        // return builder.toString().getBytes();
    }

    public static String asciiToHex(String asciiStr) {
        char[] chars = asciiStr.toCharArray();
        StringBuilder hex = new StringBuilder();
        for (char ch : chars) {
            int i = (int) ch;
            if (i < 16)
                hex.append("0");
            hex.append(Integer.toHexString(i).toUpperCase());
        }
        return hex.toString();
    }

    public static String getEmptyMessage(int maxSize) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        try {
            do{
                sb.append("00");
            } while (toAsciiBytes(sb.toString()).length < maxSize-8);
        } finally {}
        return sb.toString();
    }

    public static String binTempDecimals(double temp) {
        int units = (int) temp;
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        float decimals = Float.valueOf(decimalFormat.format(temp - units));
        // float decimals = Math.round(temp-units);
        if(decimals<0.1){
            decimals += 0.1;
        }
        String binUnits = Integer.toBinaryString(units);
        String binDec = "";
        while (binDec == "") {
            if (decMap().containsKey(decimals)) {
                binDec = decMap().get(decimals);
            } else {
                if (decimals < 0.02)
                    binDec = "000000";
                else
                    decimals = Float.valueOf(decimalFormat.format(decimals - 0.01));
            }
        }
        return binUnits.concat(binDec);
    }

    private static String dec2dms(double val, int type) throws Exception {
        StringBuilder gpsStr = new StringBuilder();
        int deg = (int) val;
        double min = (Math.abs(val) - Math.abs(deg)) * 60;
        int res = (int) min;
        double sec = (min - res) * 60;
        String minSec = String.valueOf(res).concat(String.valueOf((int)sec));
        int ms = Integer.parseInt(minSec);
        switch (type) {
            case LAT:
                if (deg < 0) {
                    deg = 128 + (deg * -1);
                    if (deg < 128 | deg > 218)
                        throw new Exception("Valor fuera de rango");
                }
                String check = Integer.toHexString(deg);
                while (check.length() < 2)
                    check = "0".concat(check);
                gpsStr.append(check);
                if (ms > 5999) {
                    throw new Exception("Valor fuera de rango");
                } else {
                    check = Integer.toHexString(ms);
                    while (check.length() < 4)
                        check = "0".concat(check);
                    gpsStr.append(check);
                }
                break;
            case LON:
                if (ms > 5999) {
                    throw new Exception("Valor fuera de rango");
                }
                if (deg > 180) {
                    throw new Exception("Valor hexadecimal de grados " + deg + " superior al máximo 180");
                }
                if (deg < 0 & deg >= -180) {
                    deg *= -1;
                    ms += 32768;
                }
                String str = Integer.toHexString(deg);
                while (str.length() < 2)
                    str = "0".concat(str);
                gpsStr.append(str);
                str = Integer.toHexString(ms);
                while (str.length() < 4)
                    str = "0".concat(str);
                gpsStr.append(str);
                break;

        }
        return gpsStr.toString();
    }

    private static HashMap<Float, String> decMap() {
        if (map == null) {
            map = new HashMap<>();
            map.put(0.02f, "000001");
            map.put(0.03f, "000010");
            map.put(0.05f, "000011");
            map.put(0.06f, "000100");
            map.put(0.08f, "000101");
            map.put(0.09f, "000110");
            map.put(0.11f, "000111");
            map.put(0.13f, "001000");
            map.put(0.15f, "001001");
            map.put(0.16f, "001010");
            map.put(0.19f, "001100");
            map.put(0.21f, "001101");
            map.put(0.22f, "001110");
            map.put(0.24f, "001111");
            map.put(0.25f, "010000");
            map.put(0.27f, "010001");
            map.put(0.28f, "010010");
            map.put(0.30f, "010011");
            map.put(0.31f, "010100");
            map.put(0.33f, "010101");
            map.put(0.34f, "010110");
            map.put(0.36f, "010111");
            map.put(0.38f, "011000");
            map.put(0.40f, "011001");
            map.put(0.41f, "011010");
            map.put(0.43f, "011011");
            map.put(0.44f, "011100");
            map.put(0.46f, "011101");
            map.put(0.47f, "011110");
            map.put(0.49f, "011111");
            map.put(0.50f, "100000");
            map.put(0.52f, "100001");
            map.put(0.53f, "100010");
            map.put(0.55f, "100011");
            map.put(0.56f, "100100");
            map.put(0.58f, "100101");
            map.put(0.61f, "100111");
            map.put(0.63f, "101000");
            map.put(0.65f, "101001");
            map.put(0.66f, "101010");
            map.put(0.68f, "101011");
            map.put(0.69f, "101100");
            map.put(0.71f, "101101");
            map.put(0.72f, "101110");
            map.put(0.74f, "101111");
            map.put(0.75f, "110000");
            map.put(0.77f, "110001");
            map.put(0.78f, "110010");
            map.put(0.80f, "110011");
            map.put(0.81f, "110100");
            map.put(0.83f, "110101");
            map.put(0.84f, "110110");
            map.put(0.86f, "110111");
            map.put(0.88f, "111000");
            map.put(0.90f, "111001");
            map.put(0.91f, "111010");
            map.put(0.93f, "111011");
            map.put(0.94f, "111100");
            map.put(0.96f, "111101");
            map.put(0.97f, "111110");
            map.put(0.99f, "111111");
        }
        return map;
    }

    public static String fakeRecords(int selected) throws Exception {
        ArrayList<FakeRecord> a = new ArrayList<>();
        long time = System.currentTimeMillis()/1000;
//        int random = App.getCounter();
        switch (selected){
            case 0:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.269327, 2.167655));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.236761, 2.274573));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.172485, 2.483010));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.110168, 2.682136));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.048580, 2.883662));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.957122, 3.158998));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.847344, 3.472446));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 39.971483, 5.600456));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 38.927851, 7.495909));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 37.841284, 9.052136));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 36.854858, 10.219602));
                break;
            case 1:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.355371, 0.465546));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.099955, -2.775448));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.451341, -5.938392));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.532177, -13.818908));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.290008, -22.669645));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.306995, -26.110016));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.480390, -35.863314));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.040246, -46.807794));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.589943, -55.339317));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.428715, -70.706642));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.691619, -74.168999));
                break;
            case 2:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.247358, 2.240382));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.213562, 2.349801));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.135516, 2.604889));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.084553, 2.769665));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.025786, 2.959541));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.888103, 3.356054));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.414232, 4.572661));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 39.534600, 6.470656));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 38.406437, 8.276445));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 37.308685, 9.706187));
                break;
            case 3:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.435367, 0.489998));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.252551, -3.778889));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.328957, -9.104096));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.770212, -17.361312));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.257530, -23.001731));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.441518, -31.606272));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.441451, -40.391805));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.700477, -52.978817));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.982124, -60.539820));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.754080, -73.901505));
                break;
            case 4:// FIXME: 09-22-19 cambiado para grabar tramas sin gps
                return "0101085D8D5D030F584B884B8A4B8F4B884B844B824B824B81";
            case 5:// FIXME: 09-27-19 tramas de prueba con pocos registros
                return "0101015D8D5D030F584B88";
            case 6:
                return "0101025D8D5D030F584B884B8A";
            case 7: // FIXME: 09-27-19 tramas de jorge
                return "0103235D8D00000F584B844B7F4B7D4B7B4B7B4B7A4B784B824B824B7D4B7A4B774B764B754B744B734B724B724B704B714B714B704B704B714B724B6E4B704B6C4B704B6E4B6E4B6E4B6E4B6D4B6C4B6D4B6B4B6C4B6B4B6B4B6B4B6B4B6B4B6B4B694B6C4B694B694B6B4B6A4B6A4B694B6A4B694B674B694B684B684B6A4B684B694B694B674B694B694B684B684B7C4B794B724B6C4B784B914BA84B994B854B7D4B784B774B744B8B4B864B814B7B4B774B754B754B734B7F";
            case 8:
                return "0103235D8D00000F584B664B5C4B574B554B564B554B534B544B534B534B524B514B534B514B524B534B534B514B544B524B534B524B524B544B5A4B704B744B684B624B5C4B5A4B5A4B594B5A4B584B594B584B564B584B584B574B584B584B584B574B584B584B584B584B584B584B584B584B584B594B584B5A4B58";
            case 9:
                return "0103235D8D00050F584B5F4B584B554B554B544B544B554B564B584B574B644B5E4B5C4B6C4B634B604B604B5F4B624B604B5E4B604B604B644B984B6B4B6F4B8F4B6E4B6C4B754B6C4B6C4B6C4B6D4B6C4B6C4B6C4B6C4B6C4B6C4B6C4B6C4B6E4B6D4B6E4B6E4B6E4B6D4B6B4B6C4B694B694B664B624B5E4B584B544B524B4F4B4A4B454B434B404B3D4B3F4B3E4B3E4B3D4B3D4B374B374B334B314B2F4B2D4B2D4B2D4B2F4B2F4B324B684B3B4B3A4B3A4B364B384B394B3A4B3C4B3C4B3D4B3D4B3F4B434B454B454B454B464B464B484B484B494B4C4B4C4B4C4B4A4B4C4B474B454B414B3D4B3B4B364B354B344B324B2F4B304B00";
            case 10:
                return "0103235D8D00054B584B5F4B584B554B554B544B544B554B564B584B574B644B5E4B5C4B6C4B634B604B604B5F4B624B604B5E4B604B604B644B984B6B4B6F4B8F4B6E4B6C4B754B6C4B6C4B6C4B6D4B6C4B6C4B6C4B6C4B6C4B6C4B6C4B6C4B6E4B6D4B6E4B6E4B6E4B6D4B6B4B6C4B694B694B664B624B5E4B584B544B524B4F4B4A4B454B434B404B3D4B3F4B3E4B3E4B3D4B3D4B374B374B334B314B2F4B2D4B2D4B2D4B2F4B2F4B324B684B3B4B3A4B3A4B364B384B394B3A4B3C4B3C4B3D4B3D4B3F4B434B454B454B454B464B464B484B484B494B4C4B4C4B4C4B4A4B4C4B474B454B414B3D4B3B4B364B354B344B324B2F4B304B00";
            case 11:
                return "0103235D8E000002584B9A4B8C4B874B874B864B864B864B854B844B854B864B854B864B844B864B854B854B884B844B874B864B854B874B864B864B874B854B864B854B864B864B884B864B864B864B864B874B884B874B874B864B874B884B864B874B864B884B884B864B884B874B874B884B864B874B884B894B884B884B894B884B874B8A4B884B884B874B8A4B874B884B88FE000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
            case 12:
                return "0103235D8E000002584B9A4B8C4B874B874B864B864B864B854B844B854B864B854B864B844B864B854B854B884B844B874B864B854B874B864B864B874B854B864B854B864B864B884B864B864B864B864B874B884B874B874B864B874B884B864B874B864B884B884B864B884B874B874B884B864B874B884B894B884B884B894B884B874B8A4B884B884B874B8A4B874B884B88FE000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
            case 13:
                return "0103235D8F5C9A02584B7E4B7D4B7A4B784B764B764B744B734B724B724B724B704B724B704B704B714B714B704B704B724B724B6E4B704B704B704B714B704B6F4B704B704B704B714B724B704B6E4B6F4B6D4B6B4B6A4B694B684B694B674B674B664B664B674B664B654B644B644B654B654B634B634B644B634B654B654B644B654B664B644B664B674B664B684B654B664B65";
//                return "05050A515CFFA90B4A8E515CFFA90D4A9C515CFFA90F4A93515CFFA9114A9F515CFFA9134A97515CFFA9154AA1515CFFA9174A98515CFFA9194AA3515CFFA91B4A98515CFFA91D4AA4";
//                return "05050A515CFFA90B4A8E2908F1020320515CFFA90D4A9C2908F903011F515CFFA90F4A932908FD020428515CFFA9114A9F2909080105ED515CFFA9134A972909150106D4515CFFA9154AA129091D0107CF515CFFA9174A982909250108B9515CFFA9194AA329092C0109A8515CFFA91B4A98290930010A97515CFFA91D4AA4290939019B86";
        }
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.269327, 2.167655));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.236761, 2.274573));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.172485, 2.483010));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.110168, 2.682136));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.048580, 2.883662));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.957122, 3.158998));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.847344, 3.472446));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 39.971483, 5.600456));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 38.927851, 7.495909));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 37.841284, 9.052136));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 36.854858, 10.219602));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.355371, 0.465546));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.099955, -2.775448));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.451341, -5.938392));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.532177, -13.818908));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.290008, -22.669645));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.306995, -26.110016));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.480390, -35.863314));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.040246, -46.807794));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.589943, -55.339317));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.428715, -70.706642));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.691619, -74.168999));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.247358, 2.240382));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.213562, 2.349801));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.135516, 2.604889));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.084553, 2.769665));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.025786, 2.959541));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.888103, 3.356054));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.414232, 4.572661));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 39.534600, 6.470656));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 38.406437, 8.276445));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 37.308685, 9.706187));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.435367, 0.489998));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.252551, -3.778889));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.328957, -9.104096));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.770212, -17.361312));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.257530, -23.001731));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.441518, -31.606272));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.441451, -40.391805));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.700477, -52.978817));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.982124, -60.539820));time+=900;
//        a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.754080, -73.901505));
        StringBuilder sb = new StringBuilder("");
        int size = a.size();
        int c = 0;
        Iterator<FakeRecord> recordIterator = a.iterator();
        sb.append("0101");
//        sb.append("0").append(selected+1).append("05");
        if(a.size()<16)
//        if(a.size()<10)
            sb.append("0");
        sb.append(Integer.toHexString(a.size()));
        sb.append(Converter.convertDate(time)).append("0F");
        int bat = getRandom(1,100);
        String s = (bat<16)?"0"+Integer.toHexString(bat):Integer.toHexString(bat);
        sb.append(s);
//        sb.append("51");
        while (recordIterator.hasNext()){
//            sb.append("5402656E");
            FakeRecord r = recordIterator.next();
//            sb.append(convertDate(time));
            sb.append(convertTemp(r.temp));
//            sb.append(convertGPS(r.lat, r.lon));// FIXME: 09-22-19 cambiado para grabar tramas sin gps
//            if(c<=size-2){
//                sb.append("51");
//            }
            c++;
            time += 900000;
        }

        return sb.toString().toUpperCase();
    }

    public static int getRandom(int min, int max) {
        int i = new Random().nextInt((max - min) + 1) + min;
        return i;
    }

    @SuppressLint("NewApi")
    public static double getRandom(double min, double max) {
//        DecimalFormat format = new DecimalFormat("#.##");
//        return Double.valueOf(format.format(ThreadLocalRandom.current().nextDouble(min, max)));
        double i = ThreadLocalRandom.current().nextDouble(min, max);
        return i;
    }

}

class FakeRecord{
    public long time;
    public double temp, lat, lon;
    FakeRecord(long time, double temp, double lat, double lon){
        this.time = time;
        this.temp = temp;
        this.lon = lon;
        this.lat = lat;
    }
}
