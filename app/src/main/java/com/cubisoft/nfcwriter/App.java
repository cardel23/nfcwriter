package com.cubisoft.nfcwriter;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by pwk04 on 07-12-19.
 */



public class App extends Application {

    private Set<String> tracks;
    private static App mContext;
    private SharedPreferences sharedPreferences;
    private static final String TAG = "NFCWriter";
    public static final String LOG_NAME = new SimpleDateFormat("yyyyMMdd").format(new Date());

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public SharedPreferences getPrefs(){
        if(sharedPreferences == null)
            sharedPreferences = getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    /**
     * Edita un parámetro de configuración (SharedPreference)
     * @param preferenceName nombre de la preferencia
     * @param val valor asignado
     * @author Carlos Delgadillo
     * @fecha 06/05/2015
     */
    public void editSharedPreference(String preferenceName, String val){
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString(preferenceName, val);
        editor.commit();
    }

    /**
     * Edita un parámetro de configuración (SharedPreference)
     * @param preferenceName
     * @param val valor asignado
     * @author Carlos Delgadillo
     * @fecha 06/05/2015
     */
    public void editSharedPreference(String preferenceName, int val){
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putInt(preferenceName, val);
        editor.commit();
    }

    public void addTrack(String string){
        getTracks().add(string);
    }

    public Set<String> getTracks(){
        return getPrefs().getStringSet("tracks", new HashSet<String>());
    }

    public static int getRandom(int min, int max){
        int i = new Random().nextInt(max - min + 1) + min;
        return i;
    }

    public static App getContext() {
        return mContext;
    }

    @SuppressLint("SimpleDateFormat")
    public void appendLog(String text)
    {
        String fileName = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+TAG+"_"+ getPrefs().getString(LOG_NAME,LOG_NAME) +".txt";
        File logFile = new File(fileName);
        if (!logFile.exists())
        {
            try
            {
                createLogFile(logFile);
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            StringBuilder sb = new StringBuilder();
            sb.append(getFormattedDate());
            sb.append(" - ");
            sb.append(text);
            buf.append(sb.toString());
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setLogName(String string){
        editSharedPreference(App.LOG_NAME, string);
    }

    private void createLogFile(File logFile) throws IOException {
        logFile.createNewFile();
        BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
        StringBuilder sb = new StringBuilder();
        sb.append("Log de aplicacion NFC\n");
        sb.append("Iniciado: "+getFormattedDate());
        sb.append("\n-----------------------------------------\n\n\n");
        buf.append(sb.toString());
        buf.newLine();
        buf.close();
    }

    @SuppressLint("SimpleDateFormat")
    private String getFormattedDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date date = new Date(System.currentTimeMillis());
        return sdf.format(date);
    }

    public void appendException(Throwable t){
        appendLog(t.getMessage());
        for(StackTraceElement i : t.getStackTrace())
            appendLog(" - " + i.getClassName() +":"+
                    String.valueOf(i.getLineNumber()));
    }

}
