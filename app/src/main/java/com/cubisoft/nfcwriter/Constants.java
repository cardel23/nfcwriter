package com.cubisoft.nfcwriter;

import java.util.HashMap;

public class Constants {

    private static final HashMap<String, String> ENTITY_NAME_MAP = new HashMap<>();
    private static final String SENSOR = "PW_Sensores";
    private static final String PRODXSHIP = "PW_ProductosXEnvio";
    private static final String PRODUCT = "AOS_Products";
    private static final String SHIPMENT= "Meeting";

    public static final String PRODUCT_IMAGES_DIRNAME = "products";

    private static final String NFC_START = "0010";
    private static final String NFC_ERROR = "001A";
    private static final String NFC_SUCCESS = "00E5";
    private static final String REQUEST_PREFIX = "6363";
    private static final String CONFIRM_PREFIX = "4242";
    private static final String BATTERY_STATUS = "0043";

    public static final String POST_NOTE_SUCCESS = "El registro ha sido guardado";
    public static final String BATT_STAT_ERR = "Error al obtener el estado de la batería: Mensaje de confirmación incorrecto";

    public static final String SIOBEX = "Cadena de tamaño incorrecto. Posible error de lectura o error de datos";

    public static final int POST_NOTE = 1, GET_CALLSIGN = 2;

    public static String getClassNames(String entityName){
//    if(ENTITY_NAME_MAP == null){
//      ENTITY_NAME_MAP = new HashMap<String,String>();

        /** Agregar los maps con los nombres de entidades y su clase respectiva **/
        ENTITY_NAME_MAP.put(SENSOR, "com.pwk.track.nfc.data.Sensor");
        ENTITY_NAME_MAP.put(PRODXSHIP, "com.pwk.track.nfc.data.PWPXE");
        ENTITY_NAME_MAP.put(PRODUCT,"com.pwk.track.nfc.data.AOSProduct");
        ENTITY_NAME_MAP.put(SHIPMENT,"com.pwk.track.nfc.data.Shipment");
//        ENTITY_NAME_MAP.put()
//      /** Llenar la lista con los maps **/
//      ENTITY_NAME_MAP.add(ENTITY_NAME_MAP);
//    }

//    for(HashMap<String, String> h : ENTITY_NAME_MAP ){

        if(ENTITY_NAME_MAP.containsKey(entityName))
            return ENTITY_NAME_MAP.get(entityName);
//    }
        return null;
    }

    public static String getStartDownload(String callsign){
        return REQUEST_PREFIX+callsign+NFC_START;
    }

    public static String getSuccessMessage(String callsign){
        return CONFIRM_PREFIX+callsign+NFC_SUCCESS;
    }

    public static String getErrorMessage(String callsign){
        return CONFIRM_PREFIX+callsign+NFC_ERROR;
    }

    public static String getBatteryRequest(String callsign){
        return REQUEST_PREFIX+callsign+BATTERY_STATUS;
    }

    public static String getBatteryConfirm(String callsign){
        int rand = App.getRandom(1,100);
        String prefix = (rand<16?"000":"00");
        return CONFIRM_PREFIX+callsign+prefix+Integer.toHexString(rand);
    }

}
